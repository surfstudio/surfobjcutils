//
//  SRFCoreDataRefCounter.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 01/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <CoreData/CoreData.h>

static NSString *const RelationshipIsOwnerKey = @"owns";

/**
 CoreDataRefCounter automatically deletes unowned objects in CoreData.
 An unowned object is an object, referenced by at least one owning relationship in model,
 but having no objects connected by these relationships.
 An owning relationship is a relationship, having key "owns" = "YES" in userInfo.
 
 If A owns B in your model, select A, select the relationship referencing B,
 edit the userInfo of this relationship, and add key "owns" = "YES".
 Root entities (not referenced by such relationships) won't be deleted by this reference counter.
 Deep hierarchical structures will be handled as well.
 All kinds of relationships are supported: one-to-one, one-to-many, many-to-one, many-to-many.
 
 CoreDataRefCounter is attached to a context by assigning context.refCountingEnabled = YES.
 The attached context.refCounter will subscribe to NSManagedObjectContextWillSaveNotification of its context,
 and will check the changed objects only (not all objects).
 
 After initializing your Core Data stack, assign refCountingEnabled = YES for your root saving context.
 In MagicalRecord it's [NSManagedObjectContext MR_rootSavingContext], but you can use any stack you like.
 You can subscribe to object deletion by assigning context.refCounter.deleteObjectsHandler.
 */
@interface SRFCoreDataRefCounter : NSObject
@property (copy, nonatomic) void (^deleteObjectsHandler)(NSSet<NSManagedObject *> *objects);
@end

@interface NSManagedObjectContext (SRFRefCounting)
@property (nonatomic) BOOL srf_refCountingEnabled;
@property (strong, nonatomic, readonly) SRFCoreDataRefCounter *srf_refCounter;
@end
