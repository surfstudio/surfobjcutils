//
//  SRFCoreDataRefCounter.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 28/03/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "SRFCoreDataRefCounter.h"
#import "SRFRuntimeProperties.h"

@implementation NSRelationshipDescription (RefCounting)

- (BOOL)srf_owns {
    NSString *value = self.userInfo[RelationshipIsOwnerKey];
    if (!value) return NO;
    if ([value isKindOfClass:[NSNumber class]] && value.boolValue == NO) return NO;
    return YES;
}

- (BOOL)srf_ownedBy {
    return self.inverseRelationship.srf_owns;
}

@end


@implementation NSManagedObject (RefCounting)

- (BOOL)srf_shouldHaveOwners {
    static NSMutableDictionary *entitiesShouldHaveOwners = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        entitiesShouldHaveOwners = [NSMutableDictionary new];
    });
    @synchronized(entitiesShouldHaveOwners) {
        NSString *key = self.entity.name;
        NSNumber *shouldHaveOwnersNum = entitiesShouldHaveOwners[key];
        if (shouldHaveOwnersNum) return shouldHaveOwnersNum.boolValue;
        BOOL shouldHaveOwners = NO;
        for (NSRelationshipDescription *rel in self.entity.relationshipsByName.allValues)
            if (rel.srf_ownedBy) {
                shouldHaveOwners = YES;
                break;
            }
        entitiesShouldHaveOwners[key] = @(shouldHaveOwners);
        return shouldHaveOwners;
    }
}

- (BOOL)srf_deleteIfOwnerless {
    if (self.deleted) return NO;
    if (!self.srf_shouldHaveOwners) return NO;
    for (NSRelationshipDescription *rel in self.entity.relationshipsByName.allValues)
        if (rel.srf_ownedBy) {
            id value = [self valueForKey:rel.name];
            if (rel.toMany) {
                if ([value count])
                    return NO;
            } else {
                if (value)
                    return NO;
            }
        }
    [self.managedObjectContext deleteObject:self];
    return YES;
}

@end




@interface SRFCoreDataRefCounter ()
@property (weak, nonatomic) NSManagedObjectContext *context;
@end

@implementation SRFCoreDataRefCounter

- (void)setContext:(NSManagedObjectContext *)context {
    if (context == _context) return;
    if (_context)
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:NSManagedObjectContextWillSaveNotification
                                                      object:_context];
    _context = context;
    if (_context)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextWillSaveWithNotification:)
                                                     name:NSManagedObjectContextWillSaveNotification
                                                   object:_context];
}

- (void)dealloc {
    self.context = nil;
}

- (void)contextWillSaveWithNotification:(NSNotification *)notif {
    NSManagedObjectContext *context = notif.object;
    [context processPendingChanges];
    if (!context.updatedObjects && !context.insertedObjects) return;
    NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    childContext.parentContext = context;
    
    [context obtainPermanentIDsForObjects:context.updatedObjects.allObjects error:nil];
    [context obtainPermanentIDsForObjects:context.insertedObjects.allObjects error:nil];
    NSMutableSet *changedObjectsInChildContext = [NSMutableSet new];
    for (NSManagedObject *obj in context.updatedObjects)
        [changedObjectsInChildContext addObject:[childContext existingObjectWithID:obj.objectID error:nil]];
    for (NSManagedObject *obj in context.insertedObjects)
        [changedObjectsInChildContext addObject:[childContext existingObjectWithID:obj.objectID error:nil]];
    
    NSMutableSet *deletedObjectsInContext = [NSMutableSet new];
    while (changedObjectsInChildContext.count) {
        [childContext performBlockAndWait:^{
            for (NSManagedObject *obj in changedObjectsInChildContext)
                if ([obj srf_deleteIfOwnerless])
                    if (_deleteObjectsHandler)
                        [deletedObjectsInContext addObject:[context existingObjectWithID:obj.objectID error:nil]];
            [childContext processPendingChanges];
            [changedObjectsInChildContext removeAllObjects];
            [changedObjectsInChildContext unionSet:childContext.updatedObjects];
            [changedObjectsInChildContext unionSet:childContext.insertedObjects];
            
            NSError *err = nil;
            [childContext save:&err];
            if (err) NSLog(@"Cannot save child context when performing reference counting");
        }];
    }
    
    if (deletedObjectsInContext.count && _deleteObjectsHandler)
        _deleteObjectsHandler(deletedObjectsInContext);
}

@end


@interface NSManagedObjectContext (SRFRefCounting_Private)
@property (strong, nonatomic, readwrite) SRFCoreDataRefCounter *srf_refCounter;
@end

@implementation NSManagedObjectContext (SRFRefCounting)
SRFSynthesizeRuntimeGetterAndSetter(srf_refCounter, setSrf_refCounter, SRFCoreDataRefCounter *, obj, value);

- (BOOL)srf_refCountingEnabled {
    return [objc_getAssociatedObject(self, @selector(srf_refCountingEnabled)) boolValue];
}

- (void)setSrf_refCountingEnabled:(BOOL)refCountingEnabled {
    if (refCountingEnabled == self.srf_refCountingEnabled) return;
    objc_setAssociatedObject(self, @selector(srf_refCountingEnabled), @(refCountingEnabled), OBJC_ASSOCIATION_RETAIN);
    if (refCountingEnabled) {
        self.srf_refCounter = [SRFCoreDataRefCounter new];
        self.srf_refCounter.context = self;
    } else {
        self.srf_refCounter.context = nil;
        self.srf_refCounter = nil;
    }
}

@end
