//
//  SRFRuntimeProperties.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 14/10/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <objc/runtime.h>

/**
 Implements accessors for a given property, using objc associated objects.
 
 Parameters objToValue and valueToObj defines boxing/unboxing for primitive types.
 
 Examples:
 synthesizeRuntimeGetterAndSetter(borderColor, setBorderColor, UIColor *, obj, value);
 synthesizeRuntimeGetterAndSetter(cornerRadius, setCornerRadius, CGFloat, [obj floatValue], @(value));
 
 **/
#define SRFSynthesizeRuntimeGetterAndSetter(getter,setter,type,objToValue,valueToObj) \
-(type)getter { id obj=objc_getAssociatedObject(self, @selector(getter)); return objToValue;} \
-(void)setter:(type)value {objc_setAssociatedObject(self, @selector(getter), valueToObj, OBJC_ASSOCIATION_RETAIN);}
