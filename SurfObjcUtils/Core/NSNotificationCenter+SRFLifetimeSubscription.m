//
//  NSNotificationCenter+SRFLifetimeSubscription.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "NSNotificationCenter+SRFLifetimeSubscription.h"
#import "NSObject+SRFOnDealloc.h"

@implementation NSNotificationCenter (SRFLifetimeSubscription)

- (void)srf_observeName:(NSString *)name object:(id)object untilObjectDies:(id)objectToWait handler:(void (^)(NSNotification *))handler {
    id observer = [self addObserverForName:name object:object queue:nil usingBlock:handler];
    typeof (self) weakSelf = self;
    [objectToWait srf_addDeallocationHandler:^{
        [weakSelf removeObserver:observer];
    }];
}

@end
