//
//  NSString+SRFUtils.m
//  SurfObjcUtils
//
//  Created by Julia Ponomareva on 09.02.15.
//  Copyright (c) 2015 Surf. All rights reserved.
//

#import "NSString+SRFUtils.h"

@implementation NSString (SRFUtils)

- (BOOL)srf_containsCharset:(NSCharacterSet *)charset {
    return [self rangeOfCharacterFromSet:charset].location != NSNotFound;
}

- (BOOL)srf_containsOnlyCharset:(NSCharacterSet *)charset {
    return ![self srf_containsCharset:charset.invertedSet];
}

- (BOOL)srf_containsOnlyWhitespaces {
    return [self srf_containsOnlyCharset:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)srf_containsOnlyLetters {
    return [self srf_containsOnlyCharset:[NSCharacterSet letterCharacterSet]];
}

- (BOOL)srf_containsOnlyDecimals {
    return [self srf_containsOnlyCharset:[NSCharacterSet decimalDigitCharacterSet]];
}

- (BOOL)srf_containsOnlyLettersAndDecimals {
    static NSCharacterSet *restrictedChars = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableCharacterSet *allowedChars = [NSMutableCharacterSet letterCharacterSet];
        [allowedChars formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
        restrictedChars = [allowedChars.copy invertedSet];
    });
    return ![self srf_containsCharset:restrictedChars];
}



- (BOOL)srf_containsSubstring:(NSString *)substring {
    if (!substring || ![substring isKindOfClass:[NSString class]])
        return NO;
    if (!substring.length)
        return YES;
    return [self rangeOfString:substring].location != NSNotFound;
}

- (NSString *)srf_stringByAppendingStringSafely:(NSString *)aString {
    if (!aString || ![aString isKindOfClass:[NSString class]])
        return self;
    return [self stringByAppendingString:aString];
}

- (NSString *)srf_substringToIndexSafely:(NSInteger)to {
    if (to > (NSInteger)self.length)
        to = (NSInteger)self.length;
    if (to < 0)
        to = 0;
    return [self substringToIndex:to];
}

@end


@implementation NSMutableString (SRFUtils)

- (void)srf_appendStringSafely:(NSString *)aString {
    if (!aString || ![aString isKindOfClass:[NSString class]])
        return;
    [self appendString:aString];
}

@end
