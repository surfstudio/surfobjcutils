//
//  NSObject+SRFOnDealloc.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (SRFOnDealloc)
- (void)srf_addDeallocationHandler:(void (^)())handler;
@end
