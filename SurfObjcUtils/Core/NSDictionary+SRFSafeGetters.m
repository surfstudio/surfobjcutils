//
//  NSDictionary+SRFSafeGetters.m
//  SurfObjcUtils
//
//  Created by Ольферук Александр on 08/10/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import "NSDictionary+SRFSafeGetters.h"

@implementation NSDictionary (SRFSafeGetters)

- (id)srf_safeValueForKey:(NSString *)key class:(Class)class {
    id value = [self valueForKey:key];
    if (![value isKindOfClass:class])
        value = nil;
    return value;
}

-(NSString *)srf_safeStringForKey:(NSString *)key {
    return [self srf_safeValueForKey:key class:[NSString class]];
}

-(NSArray *)srf_safeArrayForKey:(NSString *)key {
    return [self srf_safeValueForKey:key class:[NSArray class]];
}

- (NSArray *)srf_safeDictionaryForKey:(NSString *)key {
    return [self srf_safeValueForKey:key class:[NSDictionary class]];
}

- (double)srf_safeDoubleForKey:(NSString *)key {
    return [self srf_safeDoubleNumForKey:key].doubleValue;
}

- (NSNumber *)srf_safeDoubleNumForKey:(NSString *)key {
    id value = [self valueForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        NSScanner *scanner = [NSScanner scannerWithString:value];
        double res = 0;
        return [scanner scanDouble:&res]? @(res): nil;
    } else if ([value isKindOfClass:[NSNumber class]])
        return value;
    else
        return nil;
}

- (long long)srf_safeLongLongForKey:(NSString *)key {
    return [self srf_safeLongLongNumForKey:key].longLongValue;
}

- (NSNumber *)srf_safeLongLongNumForKey:(NSString *)key {
    id value = [self valueForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        NSScanner *scanner = [NSScanner scannerWithString:value];
        long long res = 0;
        return [scanner scanLongLong:&res] && scanner.isAtEnd? @(res): nil;
    } else if ([value isKindOfClass:[NSNumber class]]) {
        long long res = [value longLongValue];
        if (![value isEqual:@(res)])
            return nil;
        return value;
    } else
        return nil;
}

- (NSDate *)srf_safeDateForKey:(NSString *)key format:(NSString *)format {
    if (!format)
        return nil;
    
    static NSMutableDictionary *formatters = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatters = [NSMutableDictionary new];
    });
    NSDateFormatter *formatter = nil;
    @synchronized(formatters) {
        formatter = formatters[format];
        if (!formatter) {
            formatter = [NSDateFormatter new];
            formatter.dateFormat = format;
            formatters[format] = formatter;
        }
    }
    return [self srf_safeDateForKey:key formatter:formatter];
}

- (NSDate *)srf_safeDateForKey:(NSString *)key formatter:(NSDateFormatter *)formatter {
    if (!formatter)
        return nil;
    NSString *string = [self srf_safeStringForKey:key];
    if (!string)
        return nil;
    return [formatter dateFromString:string];
}

@end
