//
//  NSDictionary+SRFSafeGetters.h
//  SurfObjcUtils
//
//  Created by Ольферук Александр on 08/10/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SRFSafeGetters)

#pragma mark Safe getters

/// If the value for the key is kind of the given class, returns the value. Otherwise, returns nil.
- (id)srf_safeValueForKey:(NSString *)key class:(Class)class;

/// If the value for the key is kind of NSString, returns the value. Otherwise, returns nil.
- (NSString *)srf_safeStringForKey:(NSString *)key;

/// If the value for the key is kind of NSArrat, returns the value. Otherwise, returns nil.
- (NSArray *)srf_safeArrayForKey:(NSString *)key;

/// If the value for the key is kind of NSArrat, returns the value. Otherwise, returns nil.
- (NSArray *)srf_safeDictionaryForKey:(NSString *)key;

#pragma mark Safe parsers

/// If the value for the key is kind of NSString, parses the value with the given format.
/// If fails, returns nil.
- (NSDate *)srf_safeDateForKey:(NSString *)key format:(NSString *)format;

/// If the value for the key is kind of NSString, parses the value with the given date formatter.
/// If fails, returns nil.
- (NSDate *)srf_safeDateForKey:(NSString *)key formatter:(NSDateFormatter *)formatter;

/// If the value for the key is kind of NSString or NSNumber, parses the double value
/// and returns it. If fails, returns 0.
- (double)srf_safeDoubleForKey:(NSString *)key;

/// If the value for the key is kind of NSString or NSNumber, parses the double value,
/// wraps it to NSNumber and returns it. If fails, returns 0.
- (NSNumber *)srf_safeDoubleNumForKey:(NSString *)key;

/// If the value for the key is kind of NSString or NSNumber, parses the long long value
/// and returns it. If fails, returns 0.
- (long long)srf_safeLongLongForKey:(NSString *)key;

/// If the value for the key is kind of NSString or NSNumber, parses the long long value,
/// wraps it to NSNumber and returns it. If fails, returns 0.
- (NSNumber *)srf_safeLongLongNumForKey:(NSString *)key;

@end
