//
//  SRFCompletionContext.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 09/03/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 SRFCompletionContext is a utility to implement async tasks, which may be completed in several ways.
 It's a wrapper around completion block, which guarantees that completion will be called once and only once
 and will be destroyed after calling.
 Also, it provides an info if the completion has already been called, allows additional handlers for this calling,
 and provides a convenience method to call the preparation block before completing.
 */
@interface SRFCompletionContext : NSObject
/// Indicates whether the wrapped completion was already called.
@property (nonatomic, readonly) BOOL completed;
/// Returns a context with a given completion. If completion is nil, an empty completion will be created.
+ (instancetype)contextWithCompletion:(void (^)())completion;
/// Adds handler that will be called once just before the original completion.
/// If the receiver is already completed, the handler is called synchronously.
- (void)addHandler:(void (^)())handler;
/// Completes the receiver, calling its completion. If the receiver is already completed, nothing happens.
- (void)complete;
/// Completes the receiver, calling the given block and then calling the completion.
/// If the receiver is already completed, nothing happens, and the block is not called.
- (void)completeWithBlock:(void (^)())block;
@end
