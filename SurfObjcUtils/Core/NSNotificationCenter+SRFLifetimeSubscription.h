//
//  NSNotificationCenter+SRFLifetimeSubscription.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (SRFLifetimeSubscription)
- (void)srf_observeName:(NSString *)name
                 object:(id)object
        untilObjectDies:(id)objectToWait
                handler:(void (^)(NSNotification *note))handler;
@end
