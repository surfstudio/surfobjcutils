//
//  SRFCompletionContext.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 09/03/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "SRFCompletionContext.h"

@interface SRFCompletionContext ()
@property (copy, nonatomic) void (^completion)();
@end

@implementation SRFCompletionContext

+ (instancetype)contextWithCompletion:(void (^)())completion {
    if (!completion) completion = ^{};
    SRFCompletionContext *res = [self new];
    res.completion = completion;
    return res;
}

- (void)addHandler:(void (^)())handler {
    if (!handler) return;
    @synchronized(self) {
        if (!_completion) {
            handler();
            return;
        }
        void (^oldCompletion)() = _completion;
        _completion = ^{
            handler();
            oldCompletion();
        };
    }
}

- (void)completeWithBlock:(void (^)())block {
    @synchronized(self) {
        if (!block) block = ^{};
        if (_completion) {
            block();
            _completion();
        }
        _completion = nil;
    }
}

- (void)complete {
    [self completeWithBlock:nil];
}

- (BOOL)completed {
    return !_completion;
}

@end
