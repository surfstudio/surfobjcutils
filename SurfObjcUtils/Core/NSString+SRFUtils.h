//
//  NSString+SRFUtils.h
//  SurfObjcUtils
//
//  Created by Julia Ponomareva on 09.02.15.
//  Copyright (c) 2015 Surf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SRFUtils)

#pragma mark Checking charset presence/absence

/// Checks if the receiver contains only the given charset
- (BOOL)srf_containsOnlyCharset:(NSCharacterSet *)charset;

/// Checks if the receiver contains the given charset
- (BOOL)srf_containsCharset:(NSCharacterSet *)charset;

/// Checks if the receiver contains only whitespaces
- (BOOL)srf_containsOnlyWhitespaces;

/// Checks if the receiver contains only letters (of any country, of any case)
- (BOOL)srf_containsOnlyLetters;

/// Checks if the receiver contains only decimal numbers
- (BOOL)srf_containsOnlyDecimals;

/// Checks if the receiver contains only letters (of any country or case) and decimal numbers
- (BOOL)srf_containsOnlyLettersAndDecimals;

#pragma mark Getting and checking substrings

/// Checks if the receiver contains the given substring
- (BOOL)srf_containsSubstring:(NSString *)substring;

/// Appends the given string to the receiver. If no string provided, returns the receiver.
- (NSString *)srf_stringByAppendingStringSafely:(NSString *)aString;

/// Returns the substring to the given index. If the receiver is too short, returns the receiver.
- (NSString *)srf_substringToIndexSafely:(NSInteger)to;

@end

@interface NSMutableString (SRFUtils)

/// Appends the given string to the receiver. If no string provided, does nothing.
- (void)srf_appendStringSafely:(NSString *)aString;
@end
