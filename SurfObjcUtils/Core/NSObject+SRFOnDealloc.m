//
//  NSObject+SRFOnDealloc.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "NSObject+SRFOnDealloc.h"
#import "SRFRuntimeProperties.h"

@interface SRFDeallocationListener : NSObject
@property (nonatomic, copy) void (^onDealloc)();
@end

@implementation SRFDeallocationListener

- (void)dealloc {
    if (_onDealloc)
        _onDealloc();
}

@end

@interface NSObject ()
@property (nonatomic) NSMutableArray *srf_deallocationListeners;
@end

@implementation NSObject (SRFOnDealloc)
SRFSynthesizeRuntimeGetterAndSetter(srf_deallocationListeners, setSrf_deallocationListeners, NSMutableArray *, obj, value);

- (void)srf_addDeallocationHandler:(void (^)())handler {
    if (!self.srf_deallocationListeners)
        self.srf_deallocationListeners = [NSMutableArray new];
    
    SRFDeallocationListener *listener = [SRFDeallocationListener new];
    listener.onDealloc = handler;
    [self.srf_deallocationListeners addObject:listener];
}

@end
