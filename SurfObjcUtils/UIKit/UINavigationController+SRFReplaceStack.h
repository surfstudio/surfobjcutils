//
//  UINavigationController+SRFReplaceStack.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 02/11/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (SRFReplaceStack)

/// Creates the mutable copy of the navigation stack, performs the given block with it,
/// and applies the resulted array as a navigation stack
- (void)srf_replaceViewControllers:(void (^)(NSMutableArray<UIViewController *> *controllers))block
                          animated:(BOOL)animated;

/// Finds fromController in the navigation stack and replaces it with toController,
/// or removes it if toController is not specified
-(void)srf_replaceViewController:(UIViewController*)fromController
              withViewController:(UIViewController*)toController
                        animated:(BOOL)animated;

@end
