//
//  UIScrollView+SRFImmediateTouches.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "UIScrollView+SRFImmediateTouches.h"
#import <objc/runtime.h>

@implementation UIScrollView (SRFImmediateTouches)

- (BOOL)srf_immediateTouches {
    return [objc_getAssociatedObject(self, @selector(srf_immediateTouches)) boolValue];
}

- (void)setSrf_immediateTouches:(BOOL)srf_immediateTouches {
    objc_setAssociatedObject(self, @selector(srf_immediateTouches), @(srf_immediateTouches), OBJC_ASSOCIATION_RETAIN);
    if (srf_immediateTouches) {
        self.delaysContentTouches = NO;
        if ([self isKindOfClass:[UITableView class]] ||
            [self isKindOfClass:[UICollectionView class]]) {
            for (UIView *subview in self.subviews) {
                if ([subview isKindOfClass:[UIScrollView class]]) {
                    ((UIScrollView *)subview).delaysContentTouches = NO;
                }
            }
        }
    }
}

@end
