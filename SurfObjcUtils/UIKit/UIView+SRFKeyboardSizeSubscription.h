//
//  UIView+SRFKeyboardSizeSubscription.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 28/10/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SRFKeyboardSizeSubscription)
/**
 Gets notifications about changing keyboard frame,
 calculates the overlapping height of keyboard and the given view,
 calls the given callback within an animation block with correct timing and duration.
 
 -layoutSubviews called automatically.
 
 Subscription ends when the view is deallocated.
 **/
- (void)srf_subscribeToKeyboardSizeNotificationsWithAnimations:(void (^)(CGFloat overlappingHeight))animations;
@end
