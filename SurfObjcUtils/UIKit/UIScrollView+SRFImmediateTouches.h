//
//  UIScrollView+SRFImmediateTouches.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Allows turning off delaysContentTouches flag for the receiver
/// and it's private UIScrollViews, used by UITableView and UICollectionView
@interface UIScrollView (SRFImmediateTouches)

/// Assigning YES to this property disables delaysContentTouches for the receiver
/// and it's private UIScrollViews, used by UITableView and UICollectionView
@property (nonatomic) BOOL srf_immediateTouches;
@end
