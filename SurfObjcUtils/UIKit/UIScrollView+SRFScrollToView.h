//
//  UIScrollView+SRFScrollToView.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 15-10-19.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (SRFScrollToView)
/**
 Shows the rectangle of the specific view with -scrollRect:toVisible:.
 Can be used within animation blocks with animated = NO.
 **/
- (void)srf_scrollViewToVisible:(UIView*)view animated:(BOOL)animated;
@end
