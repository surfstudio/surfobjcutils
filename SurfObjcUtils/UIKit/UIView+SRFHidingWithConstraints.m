//
//  UIView+SRFHidingWithConstraints.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 15-10-19.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import "UIView+SRFHidingWithConstraints.h"
#import "SRFRuntimeProperties.h"

@interface UIView (SRFHidingWithConstraints_private)
@property (nonatomic,readonly,strong) NSLayoutConstraint *srf_zeroHeightConstraint;
@property (nonatomic,readonly,strong) NSLayoutConstraint *srf_zeroWidthConstraint;
@end

@implementation UIView (HidingWithConstraints)

#pragma mark Height hiding
SRFSynthesizeRuntimeGetterAndSetter(srf_zeroHeightConstraint, setSrf_zeroHeightConstraint, NSLayoutConstraint*, obj, value);

- (BOOL)srf_hiddenWithHeightConstraint {
	return !!self.srf_zeroHeightConstraint;
}

- (void)setSrf_hiddenWithHeightConstraint:(BOOL)hiddenWithHeightConstraint {
	if (hiddenWithHeightConstraint == self.srf_hiddenWithHeightConstraint) return;
    if (hiddenWithHeightConstraint) {
        self.srf_zeroHeightConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1
                                                                      constant:0];
        self.srf_zeroHeightConstraint.active = YES;
	} else {
		self.srf_zeroHeightConstraint.active = NO;
		self.srf_zeroHeightConstraint = nil;
	}
}

#pragma mark Width hiding
SRFSynthesizeRuntimeGetterAndSetter(srf_zeroWidthConstraint, setSrf_zeroWidthConstraint, NSLayoutConstraint*, obj, value);

- (BOOL)srf_hiddenWithWidthConstraint {
    return !!self.srf_zeroWidthConstraint;
}

- (void)setSrf_hiddenWithWidthConstraint:(BOOL)hiddenWithWidthConstraint {
    if (hiddenWithWidthConstraint == self.srf_hiddenWithWidthConstraint) return;
    if (hiddenWithWidthConstraint) {
        self.srf_zeroWidthConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1
                                                                     constant:0];
        self.srf_zeroWidthConstraint.active = YES;
    } else {
        self.srf_zeroWidthConstraint.active = NO;
        self.srf_zeroWidthConstraint = nil;
    }
}

@end
