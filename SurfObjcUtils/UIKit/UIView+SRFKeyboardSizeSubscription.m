//
//  UIView+SRFKeyboardSizeSubscription.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 28/10/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import "UIView+SRFKeyboardSizeSubscription.h"
#import "NSObject+SRFOnDealloc.h"

@implementation UIView (SRFKeyboardSizeSubscription)

- (void)srf_subscribeToKeyboardSizeNotificationsWithAnimations:(void (^)(CGFloat))animations {
    //implement weak/strong dance manually, to avoid dependencies from extobjcs/ReactiveCocoa
    typeof(self) weakSelf = self;
    id observer =
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillChangeFrameNotification object:nil queue:nil usingBlock:^(NSNotification *notification) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
        typeof(weakSelf) self = weakSelf;
#pragma GCC diagnostic pop
        
        if (!self.window) {
            animations(0);
            [self layoutIfNeeded];
            return;
        }
        CGRect keyboardRectInWindow = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGRect keyboardRect = [self.window convertRect:keyboardRectInWindow toView:self];
        UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
        NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
        CGFloat overlappingHeight = self.bounds.size.height - keyboardRect.origin.y;
        if (overlappingHeight < 0)
            overlappingHeight = 0;
        if (overlappingHeight > self.frame.size.height)
            overlappingHeight = self.frame.size.height;
        [UIView animateWithDuration:duration
                              delay:0
                            options:curve << 16
                         animations:^{
                             animations(overlappingHeight);
                             [self layoutIfNeeded];
                         } completion:nil];
    }];
    [self srf_addDeallocationHandler:^{
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }];
}

@end
