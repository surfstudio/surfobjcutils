//
//  SRFBorderedView.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 05/10/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

/// A decorated UIView with a border and rounded corners, configured when awoken from nib.
/// Clipping subview's corners depends on clipsToBounds property.
@interface SRFBorderedView : UIView

/// The border width, applied to the view's layer
@property (nonatomic,readwrite) IBInspectable CGFloat borderWidth;

/// The corner radius, applied to the view's layer
@property (nonatomic,readwrite) IBInspectable CGFloat cornerRadius;

/// The border color, applied to the view's layer
@property (nonatomic,readwrite,strong) IBInspectable UIColor *borderColor;
@end
