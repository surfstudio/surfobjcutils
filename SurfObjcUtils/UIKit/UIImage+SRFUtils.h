//
//  UIImage+SRFUtils.h
//  SurfObjcUtils
//
//  Created by Julia Pozdnyakova on 26.03.15.
//  Copyright (c) 2015 Surf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SRFUtils)

/// Returns a 1x1 image with the given pixel color
+ (UIImage *)srf_imageFromColor:(UIColor *)color;

/// Returns the receiver, rendered with the given alpha
- (UIImage *)srf_imageByApplyingAlpha:(CGFloat)alpha;

@end
