//
//  UIViewController+SRFContainer.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/09/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Utility to add/remove child view controllers,
 with automatic management of VC hierarchy, view hierarchy, constraints, and VC callbacks.
 
 Conforms to Apple's guide to implement container VCs:
 https://developer.apple.com/library/ios/featuredarticles/ViewControllerPGforiPhoneOS/ImplementingaContainerViewController.html
 **/
@interface UIViewController (SRFContainer)
-(void)srf_addChildViewController:(UIViewController *)childController andShowInContainerView:(UIView*)container;
-(void)srf_removeFromParentViewControllerAndHideView;
@end
