//
//  UIRefreshControl+SRFConvenientSwitching.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 28/09/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import "UIRefreshControl+SRFConvenientSwitching.h"
#import <objc/runtime.h>
#import "SRFRuntimeProperties.h"

@interface UIRefreshControl (SRFConvenientSwitching_Private)
@property (nonatomic, weak) UIScrollView *srf_referencingScrollView;
@end

@implementation UIRefreshControl (SRFConvenientSwitching)
@dynamic refreshing;

SRFSynthesizeRuntimeGetterAndSetter(srf_referencingScrollView, setSrf_referencingScrollView, UIScrollView *, obj, value);

- (void)setRefreshing:(BOOL)refreshing {
    if (refreshing == self.refreshing) return;
    if (refreshing)
        [self beginRefreshing];
    else
        [self endRefreshing];
}

- (BOOL)srf_disabledInScrollView {
    return [objc_getAssociatedObject(self, @selector(srf_disabledInScrollView)) boolValue];
}

- (void)setSrf_disabledInScrollView:(BOOL)disabledInScrollView {
    objc_setAssociatedObject(self, @selector(srf_disabledInScrollView), @(disabledInScrollView), OBJC_ASSOCIATION_RETAIN);
    if (self.srf_referencingScrollView) {
        if (disabledInScrollView && self.superview == self.srf_referencingScrollView)
            [self removeFromSuperview];
        else if (!disabledInScrollView && self.superview != self.srf_referencingScrollView)
            [self.srf_referencingScrollView addSubview:self];
    }
}

@end


@implementation UIScrollView (RefreshControl)

- (UIRefreshControl *)srf_attachedRefreshControl {
    return objc_getAssociatedObject(self, @selector(srf_attachedRefreshControl));
}

- (void)setSrf_attachedRefreshControl:(UIRefreshControl *)attachedRefreshControl {
    if (attachedRefreshControl == self.srf_attachedRefreshControl) return;
    [self.srf_attachedRefreshControl removeFromSuperview];
    self.srf_attachedRefreshControl.srf_referencingScrollView = nil;
    objc_setAssociatedObject(self, @selector(srf_attachedRefreshControl), attachedRefreshControl, OBJC_ASSOCIATION_RETAIN);
    if (attachedRefreshControl) {
        attachedRefreshControl.srf_referencingScrollView = self;
        if (!attachedRefreshControl.srf_disabledInScrollView)
            [self addSubview:attachedRefreshControl];
    }
}

@end
