//
//  UIRefreshControl+SRFConvenientSwitching.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 28/09/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Allows to configure refreshControl state by setting properties.
 **/
@interface UIRefreshControl (SRFConvenientSwitching)
@property (nonatomic,readwrite) BOOL refreshing;
@property (nonatomic,readwrite) BOOL srf_disabledInScrollView;
@end

/**
 Allows to attach the refreshControl to scrollView, without keeping a reference to it.
 **/
@interface UIScrollView (SRFRefreshControl)
@property (nonatomic,readwrite,strong) UIRefreshControl *srf_attachedRefreshControl;
@end
