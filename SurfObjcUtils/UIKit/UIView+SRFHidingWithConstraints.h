//
//  UIView+SRFHidingWithConstraints.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 15-10-19.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Manages showing/hiding views with a zero size constraints.
 **/
@interface UIView (SRFHidingWithConstraints)
///Adds/removes a zero height constraint to hide/show the given view.
@property (nonatomic,readwrite) BOOL srf_hiddenWithHeightConstraint;
///Adds/removes a zero width constraint to hide/show the given view.
@property (nonatomic,readwrite) BOOL srf_hiddenWithWidthConstraint;
@end
