//
//  SRFBorderedView.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 05/10/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import "SRFBorderedView.h"

@implementation SRFBorderedView

-(void)awakeFromNib {
    [super awakeFromNib];
    if (self.borderWidth>0)
        self.layer.borderWidth = self.borderWidth;
    if (self.borderColor)
        self.layer.borderColor = self.borderColor.CGColor;
    if (self.cornerRadius>0)
        self.layer.cornerRadius = self.cornerRadius;
}

@end
