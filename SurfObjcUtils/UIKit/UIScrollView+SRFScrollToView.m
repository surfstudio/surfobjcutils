//
//  UIScrollView+SRFScrollToView.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 15-10-19.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import "UIScrollView+SRFScrollToView.h"

@implementation UIScrollView (SRFScrollToView)

- (void)srf_scrollViewToVisible:(UIView *)view animated:(BOOL)animated {
	if (!view)
        return;
	CGRect rect = [self convertRect:view.bounds fromView:view];
	[self scrollRectToVisible:rect animated:animated];
}

@end
