//
//  UIViewController+SRFContainer.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/09/15.
//  Copyright (c) 2015 SurfStudio. All rights reserved.
//

#import "UIViewController+SRFContainer.h"

@implementation UIViewController (SRFContainer)

- (void)srf_addChildViewController:(UIViewController *)controller andShowInContainerView:(UIView *)container {
    if (!controller) return;
    if (controller.parentViewController == self) return;
    [self addChildViewController:controller];
    
    controller.view.translatesAutoresizingMaskIntoConstraints = NO;
    [container addSubview:controller.view];
#define makeEqualsConstraint(attr) [NSLayoutConstraint constraintWithItem:container attribute:attr\
 relatedBy:NSLayoutRelationEqual toItem:controller.view attribute:attr multiplier:1 constant:0]
    [NSLayoutConstraint activateConstraints:@[makeEqualsConstraint(NSLayoutAttributeLeading),
                                              makeEqualsConstraint(NSLayoutAttributeTrailing),
                                              makeEqualsConstraint(NSLayoutAttributeTop),
                                              makeEqualsConstraint(NSLayoutAttributeBottom)]];
    
    [controller.view layoutIfNeeded];
    
    [controller didMoveToParentViewController:self];
}

- (void)srf_removeFromParentViewControllerAndHideView {
    if (self.parentViewController == nil) return;
    [self willMoveToParentViewController:nil];
    
    if (self.isViewLoaded)
        [self.view removeFromSuperview];
    
    [self removeFromParentViewController];
}

@end
