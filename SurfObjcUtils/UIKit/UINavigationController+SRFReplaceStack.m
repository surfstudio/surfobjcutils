//
//  UINavigationController+SRFReplaceStack.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 02/11/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import "UINavigationController+SRFReplaceStack.h"

@implementation UINavigationController (SRFReplaceStack)

- (void)srf_replaceViewControllers:(void (^)(NSMutableArray<UIViewController *> *))block animated:(BOOL)animated {
    NSMutableArray *controllers = self.viewControllers.mutableCopy;
    block(controllers);
    [self setViewControllers:controllers animated:animated];
}

-(void)srf_replaceViewController:(UIViewController *)fromController
              withViewController:(UIViewController *)toController
                        animated:(BOOL)animated {
    [self srf_replaceViewControllers:^(NSMutableArray<UIViewController *> *controllers) {
        NSUInteger i = [controllers indexOfObject:fromController];
        if (i == NSNotFound)
            return;
        if (toController)
            [controllers replaceObjectAtIndex:i withObject:toController];
        else
            [controllers removeObjectAtIndex:i];
    } animated:animated];
}

@end
