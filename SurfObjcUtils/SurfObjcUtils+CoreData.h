//
//  SurfObjcUtils+CoreData.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 19/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <SurfObjcUtils/SurfObjcUtils.h>

#import <SurfObjcUtils/SRFCoreDataRefCounter.h>
