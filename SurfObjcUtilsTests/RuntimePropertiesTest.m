//
//  RuntimePropertiesTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 29/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SRFRuntimeProperties.h"

@interface NSNumber (TestRuntimeProperties)
@property (strong, nonatomic) NSString *sampleStringProperty;
@property (nonatomic) int sampleIntProperty;
@end

@implementation NSNumber (TestRuntimeProperties)
SRFSynthesizeRuntimeGetterAndSetter(sampleStringProperty, setSampleStringProperty, NSString *, obj, value);
SRFSynthesizeRuntimeGetterAndSetter(sampleIntProperty, setSampleIntProperty, int, [obj intValue], @(value));
@end

@interface RuntimePropertiesTest : XCTestCase

@end

@implementation RuntimePropertiesTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testProperties {
    NSNumber *obj = @1;
    XCTAssertEqualObjects(obj.sampleStringProperty, nil);
    obj.sampleStringProperty = @"asdf";
    XCTAssertEqualObjects(obj.sampleStringProperty, @"asdf");
    obj.sampleStringProperty = nil;
    XCTAssertEqualObjects(obj.sampleStringProperty, nil);
    
    XCTAssertEqual(obj.sampleIntProperty, 0);
    obj.sampleIntProperty = 10;
    XCTAssertEqual(obj.sampleIntProperty, 10);
    obj.sampleIntProperty = 0;
    XCTAssertEqual(obj.sampleIntProperty, 0);
}

@end
