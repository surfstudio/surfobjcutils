//
//  OnDeallocTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 10/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSObject+SRFOnDealloc.h"

@interface OnDeallocTest : XCTestCase

@end

@implementation OnDeallocTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testOnDealloc {
    __block int numberOfCalls = 0;
    @autoreleasepool {
        NSMutableArray *scope = [NSMutableArray new];
        @autoreleasepool {
            NSObject *obj = [NSObject new];
            [obj srf_addDeallocationHandler:^{
                numberOfCalls++;
            }];
            [scope addObject:obj];
            XCTAssertEqual(numberOfCalls, 0);
        }
        XCTAssertEqual(numberOfCalls, 0);
        scope = nil;
        XCTAssertEqual(numberOfCalls, 1);
    }
    XCTAssertEqual(numberOfCalls, 1);
}

@end
