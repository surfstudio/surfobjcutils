//
//  ImageUtilsTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIImage+SRFUtils.h"

@interface ImageUtilsTest : XCTestCase

@end



@implementation UIImage (PixelColor)

- (UIColor *)colorAtPoint:(CGPoint)pixelPoint
{
    if (pixelPoint.x > self.size.width ||
        pixelPoint.y > self.size.height) {
        return nil;
    }
    
    CGDataProviderRef provider = CGImageGetDataProvider(self.CGImage);
    CFDataRef pixelData = CGDataProviderCopyData(provider);
    const UInt8* data = CFDataGetBytePtr(pixelData);
    
    int numberOfColorComponents = 4; // R,G,B, and A
    float x = pixelPoint.x;
    float y = pixelPoint.y;
    float w = self.size.width;
    int pixelInfo = ((w * y) + x) * numberOfColorComponents;
    
    UInt8 red = data[pixelInfo];
    UInt8 green = data[(pixelInfo + 1)];
    UInt8 blue = data[pixelInfo + 2];
    UInt8 alpha = data[pixelInfo + 3];
    CFRelease(pixelData);
    
    // RGBA values range from 0 to 255
    return [UIColor colorWithRed:red/255.0
                           green:green/255.0
                            blue:blue/255.0
                           alpha:alpha/255.0];
}

@end

@implementation ImageUtilsTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testImageWithColor {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    void (^testColor)(UIColor *, NSString *) = ^(UIColor *color, NSString *name) {
        UIImage *image = [UIImage srf_imageFromColor:color];
        UIImage *sample = [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:nil]];
        XCTAssertEqualObjects(UIImagePNGRepresentation(image), UIImagePNGRepresentation(sample));
    };
    testColor([UIColor whiteColor], @"whitePixel.png");
    testColor([UIColor clearColor], @"transparentPixel.png");
}

- (void)testApplyAlpha {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    void (^testImage)(NSString *, NSString *, CGFloat) = ^(NSString *inputName, NSString *outputName, CGFloat alpha) {
        UIImage *origin = [UIImage imageWithContentsOfFile:[bundle pathForResource:inputName ofType:nil]];
        UIImage *res = [origin srf_imageByApplyingAlpha:alpha];
        UIImage *sample = [UIImage imageWithContentsOfFile:[bundle pathForResource:outputName ofType:nil]];
        XCTAssertEqualObjects(UIImagePNGRepresentation(res), UIImagePNGRepresentation(sample));
    };
    testImage(@"testImage.png", @"testImage25%.png", 0.25);
    testImage(@"whitePixel.png", @"transparentPixel.png", 0);
}

@end
