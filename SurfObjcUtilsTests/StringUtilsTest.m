//
//  StringUtilsTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+SRFUtils.h"

@interface StringUtilsTest : XCTestCase

@end

@implementation StringUtilsTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testContainsCharset {
    NSCharacterSet *myChars = [NSCharacterSet characterSetWithCharactersInString:@"abc"];
    
    XCTAssertEqual([@"asdf" srf_containsOnlyCharset:myChars], NO);
    XCTAssertEqual([@"" srf_containsOnlyCharset:myChars], YES);
    XCTAssertEqual([@"qqqq" srf_containsOnlyCharset:myChars], NO);
    XCTAssertEqual([@"ab" srf_containsOnlyCharset:myChars], YES);
    XCTAssertEqual([@" ab" srf_containsOnlyCharset:myChars], NO);
    XCTAssertEqual([@"\n" srf_containsOnlyCharset:myChars], NO);
    
    XCTAssertEqual([@"asdf" srf_containsCharset:myChars], YES);
    XCTAssertEqual([@"" srf_containsCharset:myChars], NO);
    XCTAssertEqual([@"qqqq" srf_containsCharset:myChars], NO);
    XCTAssertEqual([@"ab" srf_containsCharset:myChars], YES);
    XCTAssertEqual([@" ab" srf_containsCharset:myChars], YES);
    XCTAssertEqual([@"\n" srf_containsCharset:myChars], NO);
}

- (void)testSpecificCharsets {
    XCTAssertEqual([@" \t" srf_containsOnlyWhitespaces], YES);
    XCTAssertEqual([@"\n" srf_containsOnlyWhitespaces], NO);
    XCTAssertEqual([@"  a  " srf_containsOnlyWhitespaces], NO);
    
    XCTAssertEqual([@"asdfASDF" srf_containsOnlyLetters], YES);
    XCTAssertEqual([@"фываФЫВА" srf_containsOnlyLetters], YES);
    XCTAssertEqual([@"異體字" srf_containsOnlyLetters], YES); //chinese
    XCTAssertEqual([@"漢字仮名" srf_containsOnlyLetters], YES); //japanese
    XCTAssertEqual([@"äöü" srf_containsOnlyLetters], YES); //diacritic
    
    XCTAssertEqual([@"1234567890" srf_containsOnlyDecimals], YES);
    XCTAssertEqual([@"123E3" srf_containsOnlyDecimals], NO);
    
    XCTAssertEqual([@"1234asdfASDFфываФЫВА" srf_containsOnlyLettersAndDecimals], YES);
    XCTAssertEqual([@"123" srf_containsOnlyLettersAndDecimals], YES);
    XCTAssertEqual([@"ASDF" srf_containsOnlyLettersAndDecimals], YES);
    XCTAssertEqual([@"ASDF " srf_containsOnlyLettersAndDecimals], NO);
}

- (void)testComposing {
    XCTAssertEqual([@"123456" srf_containsSubstring:@"123"], YES);
    XCTAssertEqual([@"123456" srf_containsSubstring:@""], YES);
    XCTAssertEqual([@"" srf_containsSubstring:@""], YES);
    XCTAssertEqual([@"123456" srf_containsSubstring:@"123456"], YES);
    XCTAssertEqual([@"123456" srf_containsSubstring:@"1234567"], NO);
    XCTAssertEqual([@"" srf_containsSubstring:@"123456"], NO);
    XCTAssertEqual([@"1234456" srf_containsSubstring:@"123456"], NO);
    
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:@"456"], @"123456");
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:@""], @"123");
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:(id)@1], @"123");
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:(id)[NSNull null]], @"123");
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:nil], @"123");
    XCTAssertEqualObjects([@"" srf_stringByAppendingStringSafely:@"456"], @"456");
    XCTAssertEqualObjects([@"" srf_stringByAppendingStringSafely:@""], @"");
    XCTAssertEqualObjects([@"" srf_stringByAppendingStringSafely:(id)@1], @"");
    XCTAssertEqualObjects([@"" srf_stringByAppendingStringSafely:(id)[NSNull null]], @"");
    XCTAssertEqualObjects([@"123" srf_stringByAppendingStringSafely:nil], @"123");
    
    //The same for mutable strings
    for (NSString *firstString in @[@"123", @""])
        for (NSString *secondString in @[@"456", @1, [NSNull null]]) {
            NSMutableString *first = [NSMutableString stringWithString:firstString];
            NSString *second = (secondString == (id)[NSNull null])? nil: secondString;
            [first srf_appendStringSafely:second];
            XCTAssertEqualObjects(first, [firstString srf_stringByAppendingStringSafely:second]);
        }
    
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:20], @"1234");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:5], @"1234");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:4], @"1234");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:3], @"123");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:1], @"1");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:0], @"");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:-1], @"");
    XCTAssertEqualObjects([@"1234" srf_substringToIndexSafely:-10], @"");
    XCTAssertEqualObjects([@"" srf_substringToIndexSafely:4], @"");
    XCTAssertEqualObjects([@"" srf_substringToIndexSafely:1], @"");
    XCTAssertEqualObjects([@"" srf_substringToIndexSafely:0], @"");
    XCTAssertEqualObjects([@"" srf_substringToIndexSafely:-1], @"");
}

@end
