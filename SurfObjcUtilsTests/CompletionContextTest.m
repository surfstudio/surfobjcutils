//
//  CompletionContextTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 22/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SRFCompletionContext.h"
#import "NSObject+SRFOnDealloc.h"

@interface CompletionContextTest : XCTestCase

@end

@implementation CompletionContextTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSimpleSyncCalling {
    __block int numberOfCalls = 0;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        numberOfCalls++;
    }];
    XCTAssertEqual(numberOfCalls, 0);
    XCTAssertEqual(context.completed, NO);
    [context complete];
    XCTAssertEqual(numberOfCalls, 1);
    XCTAssertEqual(context.completed, YES);
    [context complete];
    XCTAssertEqual(numberOfCalls, 1);
    XCTAssertEqual(context.completed, YES);
}

- (void)testSimpleAsyncCalling {
    XCTestExpectation *expectation = [self expectationWithDescription:@""];
    __block int numberOfCalls = 0;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        numberOfCalls++;
    }];
    XCTAssertEqual(numberOfCalls, 0);
    XCTAssertEqual(context.completed, NO);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        XCTAssertEqual(numberOfCalls, 0);
        XCTAssertEqual(context.completed, NO);
        [context complete];
        XCTAssertEqual(numberOfCalls, 1);
        XCTAssertEqual(context.completed, YES);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            XCTAssertEqual(numberOfCalls, 1);
            XCTAssertEqual(context.completed, YES);
            [context complete];
            XCTAssertEqual(numberOfCalls, 1);
            XCTAssertEqual(context.completed, YES);
            [expectation fulfill];
        });
    });
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

- (void)testBlockAsyncCalling {
    XCTestExpectation *expectation = [self expectationWithDescription:@""];
    __block int numberOfCalls = 0;
    __block int otherNumberOfCalls = 0;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        numberOfCalls++;
    }];
    XCTAssertEqual(numberOfCalls, 0);
    XCTAssertEqual(otherNumberOfCalls, 0);
    XCTAssertEqual(context.completed, NO);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        XCTAssertEqual(numberOfCalls, 0);
        XCTAssertEqual(otherNumberOfCalls, 0);
        XCTAssertEqual(context.completed, NO);
        [context completeWithBlock:^{
            otherNumberOfCalls++;
            XCTAssertEqual(numberOfCalls, 0);
        }];
        XCTAssertEqual(numberOfCalls, 1);
        XCTAssertEqual(otherNumberOfCalls, 1);
        XCTAssertEqual(context.completed, YES);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            XCTAssertEqual(numberOfCalls, 1);
            XCTAssertEqual(otherNumberOfCalls, 1);
            XCTAssertEqual(context.completed, YES);
            [context completeWithBlock:^{
                otherNumberOfCalls++;
                XCTAssertEqual(numberOfCalls, 0);
            }];
            XCTAssertEqual(numberOfCalls, 1);
            XCTAssertEqual(otherNumberOfCalls, 1);
            XCTAssertEqual(context.completed, YES);
            [expectation fulfill];
        });
    });
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

- (void)testAdditionalHandlerCalling {
    XCTestExpectation *expectation = [self expectationWithDescription:@""];
    __block int numberOfCalls = 0;
    __block int numberOfCalls1 = 0;
    __block int numberOfCalls2 = 0;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        numberOfCalls++;
    }];
    XCTAssertEqual(numberOfCalls, 0);
    XCTAssertEqual(numberOfCalls1, 0);
    XCTAssertEqual(numberOfCalls2, 0);
    XCTAssertEqual(context.completed, NO);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [context addHandler:^{
            numberOfCalls1++;
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            XCTAssertEqual(numberOfCalls, 0);
            XCTAssertEqual(numberOfCalls1, 0);
            XCTAssertEqual(numberOfCalls2, 0);
            XCTAssertEqual(context.completed, NO);
            [context complete];
            XCTAssertEqual(numberOfCalls, 1);
            XCTAssertEqual(numberOfCalls1, 1);
            XCTAssertEqual(numberOfCalls2, 0);
            XCTAssertEqual(context.completed, YES);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                XCTAssertEqual(numberOfCalls, 1);
                XCTAssertEqual(numberOfCalls1, 1);
                XCTAssertEqual(numberOfCalls2, 0);
                XCTAssertEqual(context.completed, YES);
                [context complete];
                XCTAssertEqual(numberOfCalls, 1);
                XCTAssertEqual(numberOfCalls1, 1);
                XCTAssertEqual(numberOfCalls2, 0);
                XCTAssertEqual(context.completed, YES);
                [context addHandler:^{
                    numberOfCalls2++;
                }];
                XCTAssertEqual(numberOfCalls, 1);
                XCTAssertEqual(numberOfCalls1, 1);
                XCTAssertEqual(numberOfCalls2, 1);
                XCTAssertEqual(context.completed, YES);
                [context complete];
                XCTAssertEqual(numberOfCalls, 1);
                XCTAssertEqual(numberOfCalls1, 1);
                XCTAssertEqual(numberOfCalls2, 1);
                XCTAssertEqual(context.completed, YES);
                [expectation fulfill];
            });
        });
    });
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

- (void)testAsyncDeallocation {
    XCTestExpectation *expectation = [self expectationWithDescription:@""];
    
    @autoreleasepool {
        // In this test, you cannot just take an empty block: such block will be optimized to be static.
        // So you should define a block, capturing some variables.
        __block int someLocalVariable = 0;
        
        __block BOOL completionDeallocated = NO;
        void (^completion)() = [^{
            someLocalVariable += 1;
            someLocalVariable -= 1;
        } copy];
        [(id)completion srf_addDeallocationHandler:^{
            completionDeallocated = YES;
        }];
        
        __block BOOL contextDeallocated = NO;
        SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:completion];
        [context srf_addDeallocationHandler:^{
            contextDeallocated = YES;
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            __block BOOL handlerDeallocated = NO;
            void (^handler)() = [^{
                someLocalVariable += 2;
                someLocalVariable -= 2;
            } copy];
            [(id)handler srf_addDeallocationHandler:^{
                handlerDeallocated = YES;
            }];
            
            [context addHandler:handler];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                __block BOOL blockDeallocated = NO;
                @autoreleasepool {
                    void (^block)() = [^{
                        someLocalVariable += 3;
                        someLocalVariable -= 3;
                    } copy];
                    [(id)block srf_addDeallocationHandler:^{
                        blockDeallocated = YES;
                    }];
                    
                    [context completeWithBlock:block];
                }
                XCTAssertEqual(handlerDeallocated, YES);
                XCTAssertEqual(blockDeallocated, YES);
                XCTAssertEqual(completionDeallocated, YES);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [expectation fulfill];
                    XCTAssertEqual(contextDeallocated, YES);
                });
            });
        });
    }
    // the section above should be under @autoreleasepool, otherwise context won't die while we are waiting for expectations
    
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

@end
