//
//  SafeGettersTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+SRFSafeGetters.h"

@interface SafeGettersTest : XCTestCase

@end

@implementation SafeGettersTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSafeValues {
    NSDictionary *dict = @{@"string":@"asdf",
                           @"num":@1,
                           @"null":[NSNull null]};
    
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"string" class:[NSString class]], @"asdf");
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"num" class:[NSString class]], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"null" class:[NSString class]], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"missing" class:[NSString class]], nil);
    
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"string" class:[NSNumber class]], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"num" class:[NSNumber class]], @1);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"null" class:[NSNumber class]], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"missing" class:[NSNumber class]], nil);
    
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"string" class:nil], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:@"missing" class:nil], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:nil class:[NSString class]], nil);
    XCTAssertEqualObjects([dict srf_safeValueForKey:nil class:nil], nil);
}

- (void)testSafeStrings {
    NSDictionary *dict = @{@"string":@"asdf",
                           @"emptyString":@"",
                           @"num":@1,
                           @"null":[NSNull null]};
    
    XCTAssertEqualObjects([dict srf_safeStringForKey:@"string"], @"asdf");
    XCTAssertEqualObjects([dict srf_safeStringForKey:@"emptyString"], @"");
    XCTAssertEqualObjects([dict srf_safeStringForKey:@"num"], nil);
    XCTAssertEqualObjects([dict srf_safeStringForKey:@"null"], nil);
    XCTAssertEqualObjects([dict srf_safeStringForKey:@"missing"], nil);
    XCTAssertEqualObjects([dict srf_safeStringForKey:nil], nil);
}

- (void)testSafeArrays {
    NSArray *array = @[@1, @"asdf", [NSNull null]];
    NSDictionary *dict = @{@"array":array,
                           @"emptyArray":@[],
                           @"null":[NSNull null],
                           @"string":@"asdf"};
    
    XCTAssertEqualObjects([dict srf_safeArrayForKey:@"array"], array);
    XCTAssertEqualObjects([dict srf_safeArrayForKey:@"emptyArray"], @[]);
    XCTAssertEqualObjects([dict srf_safeArrayForKey:@"null"], nil);
    XCTAssertEqualObjects([dict srf_safeArrayForKey:@"string"], nil);
    XCTAssertEqualObjects([dict srf_safeArrayForKey:@"missing"], nil);
    XCTAssertEqualObjects([dict srf_safeArrayForKey:nil], nil);
}

- (void)testSafeDictionaries {
    NSDictionary *dict0 = @{@"asdf": @1};
    NSDictionary *dict = @{@"dict":dict0,
                           @"emptyDict":@{},
                           @"null":[NSNull null],
                           @"string":@"asdf"};
    
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:@"dict"], dict0);
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:@"emptyDict"], @{});
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:@"null"], nil);
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:@"string"], nil);
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:@"missing"], nil);
    XCTAssertEqualObjects([dict srf_safeDictionaryForKey:nil], nil);
}

- (void)testSafeDoubles {
    NSDictionary *dict = @{@"string":@"asdf",
                           @"emptyString":@"",
                           @"intString":@"-00123",
                           @"doubleString":@"-00123.123",
                           @"doubleExpString":@"-001000E-003",
                           @"intNum":@(-123),
                           @"zeroNum":@0,
                           @"doubleNum":@123.123,
                           @"null":[NSNull null]};
    
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"string"], nil);
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"emptyString"], nil);
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"intString"], @(-123));
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"doubleString"], @(-123.123));
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"doubleExpString"], @(-1));
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"intNum"], @(-123));
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"zeroNum"], @0);
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"doubleNum"], @123.123);
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"null"], nil);
    XCTAssertEqualObjects([dict srf_safeDoubleNumForKey:@"missing"], nil);
    
    XCTAssertEqual([dict srf_safeDoubleForKey:@"string"], 0);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"emptyString"], 0);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"intString"], -123);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"doubleString"], -123.123);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"doubleExpString"], -1);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"intNum"], -123);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"zeroNum"], 0);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"doubleNum"], 123.123);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"null"], 0);
    XCTAssertEqual([dict srf_safeDoubleForKey:@"missing"], 0);
}

- (void)testSafeLongLongs {
    NSDictionary *dict = @{@"string":@"asdf",
                           @"emptyString":@"",
                           @"intString":@"-00123",
                           @"doubleString":@"-00123.123",
                           @"doubleExpString":@"-001000E-003",
                           @"intNum":@(-123),
                           @"longLongNum":@9223372036854775807,
                           @"zeroNum":@0,
                           @"doubleNum":@123.123,
                           @"null":[NSNull null]};
    
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"string"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"emptyString"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"intString"], @(-123));
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"doubleString"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"doubleExpString"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"intNum"], @(-123));
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"longLongNum"], @9223372036854775807);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"zeroNum"], @0);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"doubleNum"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"null"], nil);
    XCTAssertEqualObjects([dict srf_safeLongLongNumForKey:@"missing"], nil);
    
    XCTAssertEqual([dict srf_safeLongLongForKey:@"string"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"emptyString"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"intString"], -123);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"doubleString"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"doubleExpString"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"intNum"], -123);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"longLongNum"], 9223372036854775807);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"zeroNum"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"doubleNum"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"null"], 0);
    XCTAssertEqual([dict srf_safeLongLongForKey:@"missing"], 0);
}

@end
