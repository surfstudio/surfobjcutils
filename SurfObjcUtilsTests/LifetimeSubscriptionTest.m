//
//  LifetimeSubscriptionTest.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 18/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNotificationCenter+SRFLifetimeSubscription.h"

@interface LifetimeSubscriptionTest : XCTestCase

@end

@implementation LifetimeSubscriptionTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLifetimeSubscription {
    __block int numberOfCalls = 0;
    NSString *notifName = @"testNotification";
    @autoreleasepool {
        XCTAssertEqual(numberOfCalls, 0);
        NSMutableArray *scope = [NSMutableArray new];
        [[NSNotificationCenter defaultCenter] srf_observeName:notifName object:nil untilObjectDies:scope handler:^(NSNotification *note) {
            numberOfCalls++;
        }];
        XCTAssertEqual(numberOfCalls, 0);
        [[NSNotificationCenter defaultCenter] postNotificationName:notifName object:nil];
        XCTAssertEqual(numberOfCalls, 1);
        [[NSNotificationCenter defaultCenter] postNotificationName:notifName object:nil];
        XCTAssertEqual(numberOfCalls, 2);
    }
    XCTAssertEqual(numberOfCalls, 2);
    [[NSNotificationCenter defaultCenter] postNotificationName:notifName object:nil];
    XCTAssertEqual(numberOfCalls, 2);
}

@end
