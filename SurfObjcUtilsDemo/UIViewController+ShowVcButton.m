//
//  UIViewController+ShowVcButton.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "UIViewController+ShowVcButton.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <libextobjc/extobjc.h>

@implementation UIViewController (ShowVcButton)

- (void)configureButton:(UIButton *)button toShowVc:(UIViewController *(^)())vcBlock {
    @weakify(self);
    [button bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self.navigationController pushViewController:vcBlock() animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
