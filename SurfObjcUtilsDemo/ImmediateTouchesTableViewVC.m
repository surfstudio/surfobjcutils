//
//  ImmediateTouchesTableViewVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ImmediateTouchesTableViewVC.h"

#define cellReuseIdentifier @"reuseId"

@interface ImmediateTouchesTableViewVC ()
<UITableViewDataSource,
UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ImmediateTouchesTableViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Immediate Touches: Table View";
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ImmediateTouchesTableViewCell" bundle:nil]
         forCellReuseIdentifier:cellReuseIdentifier];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
