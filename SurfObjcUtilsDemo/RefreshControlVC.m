//
//  RefreshControlVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 04/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "RefreshControlVC.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import <Masonry/Masonry.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import <libextobjc/extobjc.h>

@interface RefreshControlVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *resultsView;
@property (strong, nonatomic) IBOutlet UIView *hudView;
@property (strong, nonatomic) IBOutlet UIView *noResultsView;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (weak, nonatomic) UIView *currentView;
@property (strong, nonatomic) id currentRequest;
@property (strong, nonatomic) NSString *result;
@property (nonatomic) BOOL cannotLoad;
@end

@implementation RefreshControlVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Refresh Conrol";
    
    self.scrollView.srf_attachedRefreshControl = [UIRefreshControl new];
    @weakify(self);
    [self.scrollView.srf_attachedRefreshControl bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self reloadAll];
    } forControlEvents:UIControlEventValueChanged];
    
    [self reloadAll];
}

- (void)refreshUi {
    UIView *oldCurrentView = self.currentView;
    self.currentView = (self.result? self.resultsView:
                        self.cannotLoad? self.noResultsView:
                        self.hudView);
    if (self.currentView != oldCurrentView) {
        [oldCurrentView removeFromSuperview];
        [self.scrollView addSubview:self.currentView];
        [self.currentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.scrollView);
            make.width.equalTo(self.scrollView);
            //everything except resultsView will have fixed height
            if (self.currentView != self.resultsView)
                make.height.equalTo(self.scrollView);
        }];
    }
    
    if (self.result) {
        self.resultLabel.text = self.result;
    }
    
    BOOL unscrollableHud = self.currentView == self.hudView;
    self.scrollView.bounces = !unscrollableHud;
    self.scrollView.srf_attachedRefreshControl.srf_disabledInScrollView = unscrollableHud;
    self.scrollView.srf_attachedRefreshControl.refreshing = self.currentRequest && !unscrollableHud;
    
    [self.view layoutIfNeeded];
}

- (void)reloadAll {
    if (self.currentRequest) return;
    self.currentRequest = [NSObject new];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        BOOL success = arc4random() % 2 == 0;
        self.cannotLoad = !success;
        if (success) {
            //generate random text
            NSMutableString *result = [NSMutableString new];
            int count = arc4random() % 1000 + 1000;
            for (int i = 0; i < count; i++)
                [result appendString:(arc4random() % 5)?
                 [@"абвгдеёжзийклмнопрстуфхцчшщъыьэюя" substringWithRange:NSMakeRange(arc4random() % 33, 1)]:
                 @" "];
            self.result = result.copy;
        }
        self.currentRequest = nil;
        [self refreshUi];
    });
    [self refreshUi];
}

@end
