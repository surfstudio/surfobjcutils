//
//  BorderedViewVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "BorderedViewVC.h"

@interface BorderedViewVC ()

@end

@implementation BorderedViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Bordered View";
}

@end
