//
//  ScrollToViewVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 10/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ScrollToViewVC.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <libextobjc/extobjc.h>
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>

@interface ScrollToViewVC ()
@end

@implementation ScrollToViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Scroll to View";
}

@end

@interface ScrollToViewButton : UIButton
@property (weak, nonatomic) IBOutlet UIView *targetView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation ScrollToViewButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    @weakify(self);
    [self bk_addEventHandler:^(UIButton *button) {
        @strongify(self);
        [self.scrollView srf_scrollViewToVisible:self.targetView animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
