//
//  CDRC_C+CoreDataProperties.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRC_C+CoreDataProperties.h"

@implementation CDRC_C (CoreDataProperties)

@dynamic id_;
@dynamic bs;

@end
