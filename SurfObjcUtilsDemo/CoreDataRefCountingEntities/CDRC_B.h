//
//  CDRC_B.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDRC_A, CDRC_C;

NS_ASSUME_NONNULL_BEGIN

@interface CDRC_B : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CDRC_B+CoreDataProperties.h"
