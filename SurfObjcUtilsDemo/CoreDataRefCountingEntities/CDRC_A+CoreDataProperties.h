//
//  CDRC_A+CoreDataProperties.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRC_A.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDRC_A (CoreDataProperties)

@property (nonatomic) int64_t id_;
@property (nullable, nonatomic, retain) NSSet<CDRC_B *> *bs;

@end

@interface CDRC_A (CoreDataGeneratedAccessors)

- (void)addBsObject:(CDRC_B *)value;
- (void)removeBsObject:(CDRC_B *)value;
- (void)addBs:(NSSet<CDRC_B *> *)values;
- (void)removeBs:(NSSet<CDRC_B *> *)values;

@end

NS_ASSUME_NONNULL_END
