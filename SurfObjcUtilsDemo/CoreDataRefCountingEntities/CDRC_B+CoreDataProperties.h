//
//  CDRC_B+CoreDataProperties.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRC_B.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDRC_B (CoreDataProperties)

@property (nonatomic) int64_t id_;
@property (nullable, nonatomic, retain) CDRC_A *a;
@property (nullable, nonatomic, retain) CDRC_C *c;

@end

NS_ASSUME_NONNULL_END
