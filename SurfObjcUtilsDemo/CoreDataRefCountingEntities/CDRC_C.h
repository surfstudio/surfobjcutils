//
//  CDRC_C.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDRC_B;

NS_ASSUME_NONNULL_BEGIN

@interface CDRC_C : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CDRC_C+CoreDataProperties.h"
