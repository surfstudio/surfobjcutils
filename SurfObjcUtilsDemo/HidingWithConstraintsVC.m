//
//  HidingWithConstraintsVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 04/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "HidingWithConstraintsVC.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import <libextobjc/extobjc.h>

@interface HidingWithConstraintsVC ()
@property (weak, nonatomic) IBOutlet UIButton *showVerButton;
@property (weak, nonatomic) IBOutlet UIButton *hideVerButton;
@property (weak, nonatomic) IBOutlet UIButton *showHorButton;
@property (weak, nonatomic) IBOutlet UIButton *hideHorButton;
@property (weak, nonatomic) IBOutlet UIView *verContainer;
@property (weak, nonatomic) IBOutlet UIView *horContainer;
@end

@implementation HidingWithConstraintsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureButton:self.showVerButton toSetHidden:NO vertically:YES];
    [self configureButton:self.hideVerButton toSetHidden:YES vertically:YES];
    [self configureButton:self.showHorButton toSetHidden:NO vertically:NO];
    [self configureButton:self.hideHorButton toSetHidden:YES vertically:NO];
}

- (void)configureButton:(UIButton *)button toSetHidden:(BOOL)hidden vertically:(BOOL)vertically {
    @weakify(self);
    [button bk_addEventHandler:^(id sender) {
        @strongify(self);
        UIView *container = vertically? self.verContainer: self.horContainer;
        [UIView animateWithDuration:0.5 animations:^{
            if (vertically)
                container.srf_hiddenWithHeightConstraint = hidden;
            else
                container.srf_hiddenWithWidthConstraint = hidden;
            [container.superview layoutIfNeeded];
        }];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
