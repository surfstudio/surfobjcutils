//
//  ImmediateTouchesCollectionViewVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ImmediateTouchesCollectionViewVC.h"

@interface ImmediateTouchesCollectionViewVC ()
<UICollectionViewDataSource,
UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

#define cellReuseIdentifier @"cellId"

@implementation ImmediateTouchesCollectionViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Immediate Touches: Collection View";
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImmediateTouchesCollectionViewCell" bundle:nil]
          forCellWithReuseIdentifier:cellReuseIdentifier];
}

#pragma mark UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 19;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellReuseIdentifier
                                                                           forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

@end
