//
//  UIViewController+ShowVcButton.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ShowVcButton)
- (void)configureButton:(UIButton *)button toShowVc:(UIViewController *(^)())vcBlock;
@end
