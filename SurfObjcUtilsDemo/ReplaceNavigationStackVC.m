//
//  ReplaceNavigationStackVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 29/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ReplaceNavigationStackVC.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import <libextobjc/extobjc.h>
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>

@interface ReplaceNavigationStackVC ()
@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (weak, nonatomic) IBOutlet UIButton *pushButton;
@property (weak, nonatomic) IBOutlet UIButton *replacePreviousButton;
@property (weak, nonatomic) IBOutlet UIButton *removePreviousButton;
@property (weak, nonatomic) IBOutlet UIButton *replaceCurrentButton;
@property (weak, nonatomic) IBOutlet UIButton *removeCurrentButton;
@end

@implementation ReplaceNavigationStackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Replace Navigation Stack";
    
    static int index = 0;
    self.indexLabel.text = [@(index++) stringValue];
    
    @weakify(self);
    [self.pushButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        UIViewController *vc = [self.class new];
        // an exotic way to push view controller
        [self.navigationController srf_replaceViewControllers:^(NSMutableArray<UIViewController *> *controllers) {
            [controllers addObject:vc];
        } animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.replacePreviousButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        if (self.navigationController.viewControllers.count < 2) return;
        UIViewController *prevVc =
        self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2];
        UIViewController *vc = [self.class new];
        [self.navigationController srf_replaceViewController:prevVc withViewController:vc animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.removePreviousButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        if (self.navigationController.viewControllers.count < 2) return;
        UIViewController *prevVc =
        self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2];
        [self.navigationController srf_replaceViewController:prevVc withViewController:nil animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.replaceCurrentButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        UIViewController *curVc = self.navigationController.topViewController;
        UIViewController *vc = [self.class new];
        [self.navigationController srf_replaceViewController:curVc withViewController:vc animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.removeCurrentButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        UIViewController *curVc = self.navigationController.topViewController;
        [self.navigationController srf_replaceViewController:curVc withViewController:nil animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
