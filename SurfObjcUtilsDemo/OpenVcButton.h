//
//  OpenVcButton.h
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenVcButton : UIButton
@property (strong, nonatomic) IBInspectable NSString *vcClassName;
@end
