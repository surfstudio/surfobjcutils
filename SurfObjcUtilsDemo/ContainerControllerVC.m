//
//  ContainerControllerVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 04/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ContainerControllerVC.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import "ContainerControllerSubVC.h"

@interface ContainerControllerVC ()
@property (weak, nonatomic) IBOutlet UIView *topContainer;
@property (weak, nonatomic) IBOutlet UIView *bottomContainer;
@end

@implementation ContainerControllerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Container Controller";
    
    UIViewController *topVc = [ContainerControllerSubVC new];
    [self srf_addChildViewController:topVc andShowInContainerView:self.topContainer];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIViewController *bottomVc = [ContainerControllerSubVC new];
        [self srf_addChildViewController:bottomVc andShowInContainerView:self.bottomContainer];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [bottomVc srf_removeFromParentViewControllerAndHideView];
        });
    });
}

@end
