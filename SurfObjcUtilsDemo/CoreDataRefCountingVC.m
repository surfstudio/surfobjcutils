//
//  CoreDataRefCountingVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 23/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CoreDataRefCountingVC.h"
#import <MagicalRecord/MagicalRecord.h>
#import <SurfObjcUtils/SurfObjcUtils+CoreData.h>

#import "CDRC_A+CoreDataProperties.h"
#import "CDRC_B+CoreDataProperties.h"
#import "CDRC_C+CoreDataProperties.h"

@interface CoreDataRefCountingVC ()
@property (weak, nonatomic) IBOutlet UITextView *logView;

@property (strong, nonatomic) AutoMigratingMagicalRecordStack *stack;
@end

@implementation CoreDataRefCountingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.stack = [AutoMigratingMagicalRecordStack stackWithStoreNamed:@"CoreDataRefCountingModel"];
    [MagicalRecordStack setDefaultStack:_stack];
    _stack.context.srf_refCountingEnabled = YES;
    [_stack.context.srf_refCounter setDeleteObjectsHandler:^(NSSet<NSManagedObject *> *objects) {
        NSMutableArray *objNames = [NSMutableArray new];
        for (NSManagedObject *obj in objects)
            [objNames addObject:[NSString stringWithFormat:@"%@%@",
                                 [obj.entity.name substringWithRange:NSMakeRange(obj.entity.name.length - 1, 1)],
                                  [obj valueForKey:@"id_"]]];
        [objNames sortUsingSelector:@selector(compare:)];
        NSLog(@"Auto-deleted objects: %@", [objNames componentsJoinedByString:@", "]);
    }];
    
    [self refreshTable];
}

- (void)dealloc {
    [MagicalRecordStack setDefaultStack:nil];
    [_stack reset];
}

- (void)refreshTable {
    NSMutableString *log = [NSMutableString new];
    for (CDRC_A *a in [CDRC_A MR_findAllSortedBy:@"id_" ascending:YES]) {
        [log appendFormat:@"A%lld\n", a.id_];
        for (CDRC_B *b in a.bs)
            [log appendFormat:@"\towns B%lld\n", b.id_];
    }
    for (CDRC_B *b in [CDRC_B MR_findAllSortedBy:@"id_" ascending:YES]) {
        [log appendFormat:@"B%lld\n", b.id_];
        [log appendFormat:@"\towns C%lld\n", b.c.id_];
    }
    for (CDRC_A *c in [CDRC_C MR_findAllSortedBy:@"id_" ascending:YES]) {
        [log appendFormat:@"C%lld\n", c.id_];
    }
    self.logView.text = log;
}

- (IBAction)generateHierarchy {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        [CDRC_A MR_truncateAllInContext:context];
        [CDRC_B MR_truncateAllInContext:context];
        [CDRC_C MR_truncateAllInContext:context];
        
        int aCount = 3;
        int bCount = 5;
        int cCount = 3;
        NSMutableArray *aObjects = [NSMutableArray new];
        for (int i = 0; i < aCount; i++) {
            CDRC_A *a = [CDRC_A MR_createEntityInContext:context];
            a.id_ = i + 1;
            [aObjects addObject:a];
        }
        
        NSMutableArray *cObjects = [NSMutableArray new];
        for (int i = 0; i < aCount; i++) {
            CDRC_C *c = [CDRC_C MR_createEntityInContext:context];
            c.id_ = i + 1;
            [cObjects addObject:c];
        }
        
        for (int i = 0; i < bCount; i++) {
            CDRC_B *b = [CDRC_B MR_createEntityInContext:context];
            b.id_ = i + 1;
            b.a = aObjects[arc4random() % aCount];
            b.c = cObjects[arc4random() % cCount];
        }
    } completion:^(BOOL success, NSError * _Nullable error) {
        [self refreshTable];
    }];
}

- (IBAction)removeOneOfAObjects {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSArray<CDRC_A *> *aObjects = [CDRC_A MR_findAllInContext:context];
        if (aObjects.count)
            [aObjects[arc4random() % aObjects.count] MR_deleteEntity];
    } completion:^(BOOL success, NSError * _Nullable error) {
        [self refreshTable];
    }];
}

@end
