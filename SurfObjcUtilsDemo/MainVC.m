//
//  MainVC.m
//  SurfObjcUtilsDemo
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "MainVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"SurfObjcUtils Demo";
}

@end
