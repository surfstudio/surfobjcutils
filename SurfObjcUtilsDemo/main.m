//
//  main.m
//  SurfObjcUtilsDemo
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
