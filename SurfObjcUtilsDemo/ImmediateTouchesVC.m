//
//  ImmediateTouchesVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ImmediateTouchesVC.h"

@interface ImmediateTouchesVC ()

@end

@implementation ImmediateTouchesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Immediate Touches";
}

@end
