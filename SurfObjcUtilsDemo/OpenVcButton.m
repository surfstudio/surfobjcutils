//
//  OpenVcButton.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "OpenVcButton.h"
#import <libextobjc/extobjc.h>
#import <BlocksKit/BlocksKit+UIKit.h>

@implementation OpenVcButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if (!self.vcClassName) return;
    Class vcClass = NSClassFromString(self.vcClassName);
    if (!vcClass) return;
    
    @weakify(self);
    [self bk_addEventHandler:^(id sender) {
        @strongify(self);
        
        UIViewController *owner = ^id{
            UIResponder *res = self;
            while (res && ![res isKindOfClass:[UIViewController class]])
                res = res.nextResponder;
            return res;
        }();
        if (!owner) return;
        
        [owner.navigationController pushViewController:[vcClass new] animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
