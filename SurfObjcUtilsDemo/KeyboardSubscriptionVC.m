//
//  KeyboardSubscriptionVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 10/05/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "KeyboardSubscriptionVC.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import <libextobjc/extobjc.h>

@interface KeyboardSubscriptionVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *hideKeyboardButton;

@property (weak, nonatomic) IBOutlet UIView *anotherContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *anotherHeightConstraint;
@end

@implementation KeyboardSubscriptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Keyboard Subscription";
    
    @weakify(self);
    [self.view srf_subscribeToKeyboardSizeNotificationsWithAnimations:^(CGFloat overlappingHeight) {
        @strongify(self);
        self.keyboardHeightConstraint.constant = overlappingHeight;
    }];
    
    [self.anotherContainer srf_subscribeToKeyboardSizeNotificationsWithAnimations:^(CGFloat overlappingHeight) {
        @strongify(self);
        self.anotherHeightConstraint.constant = overlappingHeight;
    }];
    
    [self.hideKeyboardButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self.view endEditing:YES];
    } forControlEvents:UIControlEventTouchUpInside];
}

@end
