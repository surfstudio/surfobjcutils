//
//  AppDelegate.m
//  SurfObjcUtilsDemo
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "AppDelegate.h"
#import "MainVC.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    MainVC *mainVc = [MainVC new];
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:mainVc];
    navVc.navigationBar.translucent = NO;
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = navVc;
    [self.window makeKeyAndVisible];
    
    // Override point for customization after application launch.
    return YES;
}

@end
