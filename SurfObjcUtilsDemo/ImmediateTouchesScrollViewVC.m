//
//  ImmediateTouchesScrollViewVC.m
//  SurfObjcUtils
//
//  Created by Dmitrii Trofimov on 25/04/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ImmediateTouchesScrollViewVC.h"

@interface ImmediateTouchesScrollViewVC ()

@end

@implementation ImmediateTouchesScrollViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Immediate Touches: Scroll View";
}

@end
