# SurfObjcUtils

Это библиотека с Objective-C утилитами (в основном — классами и категориями), которые созданы в Surf и подготовлены к переиспользованию в виде пода.

## Если хотите добавить сюда свою утилиту

* Убедитесь, что утилита действительно переиспользуемая, т. е. не зависит от проектно-специфичных вещей. Если у вас есть, например, замечательный механизм показа всплывающих сообщений, то нужно учитывать, что в другом проекте эти сообщения будут всплывать с другой анимацией, выравниваться относительно других элементов интерфейса, будут иметь другие разновидности и другую вёрстку. В этом случае вы убьёте слишком много времени на построение хорошего абстрактного интерфейса с кучей настроек, который в итоге перевесит по размеру возможности самой утилиты.
* Убедитесь, что подключение утилиты никак не повлияет на работу проекта. Если вы используете method swizzling для изменения функциональности уже существующих классов, убедитесь, что по дефолту вызывается "родительский" метод, либо подмена методов делается только по указанию пользователя. Например: `[SRFAwesomeUtility startDoingMagic]`.
* В [нашем каталоге переиспользуемого кода](http://wiki.surfstudio.ru/Reusable_iOS_Code) добавьте соответствующий раздел. Кратко опишите фичи утилиты, полную документацию давать не нужно. Перечислите историю утилиты — кто и когда её делал, как она кочевала из проекта в проект, где искать актуальную версию. Используйте тот же формат описания, что и у остальных утилит.
* Добавьте префикс `SRF` в классы, методы и свойства, добавляемые утилитой.
	* Классы: `SRFSomeClass`. Их свойства и методы — без префикса.
	* Категории: `UIView+SRFSomeCategory`. Их свойства и методы: `srf_someProperty`, `srf_someMethod`.
	* Функции: `SRFSomeFunction`.
	* Макросы: "функциональные" — `SRFSomeMacro(param)`, "константные" — `SRF_SOME_CONSTANT`.
* Напишите пример использования вашей утилиты.
	* Юнит-тест — если результат можно проверить не глазами, и если не требуется развёртывания специальной среды / стека / контекста. Или требуется, но вам не лень.
	* Страница в демо-проекте — если утилиту можно локально использовать на отдельной странице.
	* Отдельный демо-проект — если нужно разворачивать свой стек технологий (CoreData, например).
* В этом readme напишите полную документацию по вашей утилите, включая все тонкости, которые вы подразумеваете "по умолчанию".
* В заголовочном файле утилиты добавьте документирующие комментарии (самый простой способ — комментарий с тремя слешами), по возможности по-английски. В идеале эти комментарии должны вкратце вводить в курс дела даже пользователя, который не читал readme.
* Не забудьте включить утилиту в публичный заголовок `SurfObjcUtils.h`, ссылаясь на неё через угловые скобки: `#import <SurfObjcUtils/SRFAwesomeUtility.h>`. Соответствующий хедер должен быть публичным (выбираем хедер → File inspector → Target Membership → SurfObjcUtils → выпадающий список справа → Public).

## Установка

SurfObjcUtils ставится через [CocoaPods](https://github.com/cocoapods/cocoapods). В [главной репе с подспеками](https://github.com/CocoaPods/Specs) такого пода нет. Есть два варианта установки.

**Вариант 1**. [В явном виде указываем ссылку](https://guides.cocoapods.org/using/the-podfile.html#from-a-podspec-in-the-root-of-a-library-repo) на Bitbucket и нужный тэг (ветку, коммит):

	pod 'SurfObjcUtils', :git => 'https://bitbucket.org/surfstudio/surfobjcutils', :tag => '0.0.1'

Обычная система обновления до последней минорной версии здесь не работает, т. е. если хотим обновиться с 0.0.1 до последнего 0.0.x — открываем PodFile и указываем нужную версию. Зато можно указать любой нужный коммит, даже если он ещё не вышел в новую версию.

**Вариант 2**. В начале Podfile указываем два источника подспеков - [главную репу с подспеками](https://github.com/CocoaPods/Specs) и корпоративную репу [SurfPods](https://bitbucket.org/surfstudio/surfpods):

	source 'https://bitbucket.org/surfstudio/surfpods.git'
	source 'https://github.com/CocoaPods/Specs.git'

После этого можно обычным образом ссылаться на под по его имени и версии. Система обновления подов работает как обычно.

	pod 'SurfObjcUtils', '~> 0.0.1'

## Инструкции по конкретным утилитам в алфавитном порядке

### NSDictionary+SRFSafeGetters

Категория для безопасного получения значений из `NSDictionary` с проверкой типов.

* Просто проверка типов.
	* Универсальный метод `-⁠srf_safeValueForKey:class:` извлекает значение, проверяет его на принадлежность указанному типу и возвращает его. В случае неудачи возвращает `nil`.
	* Методы `srf_safeStringForKey`, `srf_safeArrayForKey`, `srf_safeDictionaryForKey` делают то же самое, но для конкретных типов.
* Парсинг примитивных значений и дат.
	* Методы `-⁠srf_safeDateForKey:format:` и `-⁠srf_safeDateForKey:formatter:` берут строку и парсят её с указанным форматом. Для метода, принимающего формат, экземпляры `NSDateFormatter` кэшируются и переиспользуются для одного и того же формата.
	* Методы `-⁠srf_safeDoubleForKey:`, `-⁠srf_safeDoubleNumForKey:`, `-⁠srf_safeLongLongForKey:`, `-⁠srf_safeLongLongNumForKey:` парсят `double` и `long long` соответственно, корректно принимая как `NSNumber`, так и `NSString`. В случае неудачи возвращается `0` или `nil` соответственно.

Если нужно значение по умолчанию, отличное от `nil`, рекомендуется сокращённый тернарный оператор:

```objc
NSString *myString = [dict srf_safeStringForKey:@"myKey"]?:@"myDefaultValue";
```

### NSNotificationCenter+SRFLifetimeSubscription

* Метод `-⁠srf_observeName:object:untilObjectDies:handler:` делает подписку на уведомления данного `NSNotificationCenter` через блоки.
* Подписка автоматически прекращается, когда указанный объект (чаще всего — подписчик) умирает.
* Позволяет не разносить в разные места кода логику, ответственную за подписку и отписку.

### NSObject+SRFOnDealloc

Метод `-⁠srf_addDeallocationHandler:` позволяет подписываться на уничтожение конкретного объекта. Указанный блок вызовется в момент его уничтожения.

При подписке целевой объект через associated objects становится владельцем вспомогательного объекта, который хранит указанный блок, умирает вместе с целевым объектом и вызывает указанный блок при смерти. Сомнительные техники вроде method swizzling не используются.

В ReactiveCocoa / RXSwift есть аналогичная подписка, так что если вы используете их в своём проекте, можете ею воспользоваться.

### NSString+SRFUtils

* Методы `-⁠srf_containsOnlyCharset:` и `-⁠srf_containsCharset:` проверяют, что строка целиком состоит из указанных символов / содержит хотя бы один указанный символ соответственно.
* Методы `-⁠srf_containsOnlyWhitespaces`, `-⁠srf_containsOnlyLetters`, `-⁠srf_containsOnlyDecimals`, `-⁠srf_containsOnlyLettersAndDecimals` делают то же самое для конкретных чарсетов.
* Метод `-⁠srf_containsSubstring:` проверяет наличие указанной подстроки.
* Методы `-⁠srf_stringByAppendingStringSafely:`, `-⁠srf_substringToIndexSafely:`, `-⁠srf_appendStringSafely:` являются безопасными обёртками соответствующих методов `NSString` и `NSMutableString`, т. е. не падают при выходе за границу строки.

### SRFBorderedView

Вьюха с настраиваемымой границей и радиусом углов.

* Свойства `borderWidth`, `cornerRadius`, `borderColor` помечены IBInspectable, так что их можно конфигурировать из Interface Builder.
* Во время `-⁠awakeFromNib` указанные свойства применяются к `CALayer` вьюхи.

Если требуется более сложное декорирование, рекомендуется делать свой подкласс `UIView`. Пример можно посмотреть [здесь](http://bynomial.com/blog/?p=52).

### SRFCompletionContext

Класс, оборачивающий completion-блок и управляющий его состоянием. Полезен при реализации асинхронных задач, которые могут завершаться несколькими способами и подразумевают логику вида "если задача ещё не завершена, завершить её".

* Хранит указанный `completion`, пока задачу не завершат методами `-⁠complete` или `-⁠completeWithBlock:`. После этого `completion` уничтожается во избежание утечек памяти.
* Методы `-⁠complete` и `-⁠completeWithBlock:` завершают задачу только в том случае, если она ещё не завершена. В противном случае ничего не происходит.
* Метод `-⁠completeWithBlock:` вызывает указанный блок непосредственно перед завершением, если задача ещё не завершена. В противном случае ничего не происходит и блок не вызывается.
* Свойство `completed` показывает, завершена ли задача.
* Метод `-⁠addHandler:` позволяет отдельно подписаться на завершение задачи. Если задача уже завершена, вызывается сразу.

Пример использования.

```objc
- (void)doSomeRequestWithin30SecondsWithCompletion:(void (^)(int someOutputParameter))completion {
    __block NSObject *someBigResourceThatShouldBeCleared = [[NSObject alloc] init];
    [self startDoingSomeActivity];
    
    __block int someOutputParameter = 0;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        [self stopDoingSomeActivity];
        someBigResourceThatShouldBeCleared = nil;
        completion(someOutputParameter);
    }];
    
    // launch the first sequence that may complete the task
    [self doSomeRequestWithCompletion:^{
        [context completeWithBlock:^{
            someOutputParameter = 42;
        }];
    }];
    // launch the second sequence that may complete the task
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [context completeWithBlock:^{
            someOutputParameter = 24;
        }];
    });
    
    // in some other place, you can start and stop another activity to be done during the task
    if (self.shouldPerformAnotherActivity) {
        [self startDoingAnotherActivity];
        [context addHandler:^{
            [self stopDoingAnotherActivity];
        }];
    }
}
```

### SRFRuntimeProperties

Макрос для генерации аксессоров (геттера и сеттера) для свойства, хранимого в associated objects. Такое хранение часто используется при добавлении новых свойств к существующим классам.

Параметры макроса:

* геттер;
* сеттер;
* тип (для ссылочных типов звёздочку не забываем);
* выражение, преобразующее хранимое значение `obj` типа `id` в значение свойства;
* выражение, преобразующее значение свойства `value` в хранимое значение типа `id`.

Пример.

```objc
@interface SomeExistingClass (MyExtension)
@property (strong, nonatomic) NSArray *myArray;
@property (nonatomic) int myInt;
...
@end

@implementation SomeExistingClass (MyExtension)
SRFSynthesizeRuntimeGetterAndSetter(myArray, setMyArray, NSArray *, obj, value);
SRFSynthesizeRuntimeGetterAndSetter(myInt, setMyInt, int, [obj intValue], @(value));
...
@end
```

### UIImage+SRFUtils

* Генерация однопиксельной картинки. Полезно для однотонных кнопок, меняющих цвет фона при нажатии и в других состояниях:

```objc
[myButton setBackgroundImage:[UIImage srf_imageFromColor:normalStateColor] forState:UIControlStateNormal];
[myButton setBackgroundImage:[UIImage srf_imageFromColor:pressedStateColor] forState:UIControlStateHighlighted];
```

* Наложение прозрачности на нужную картинку. Масштаб картинки (@2x, @3x) сохраняется. Тоже полезно для задания состояний кнопок:

```objc
[myButton setImage:myIcon forState:UIControlStateNormal];
[myButton setImage:[myIcon srf_imageByApplyingAlpha:0.5] forState:UIControlStateHighlighted];
```

### UINavigationController+SRFReplaceStack

Метод `-⁠srf_replaceViewControllers:animated:` предоставляет удобный интерфейс для изменения навигейшн-стека, представляя его, как `NSMutableArray <UIViewcontroller *>`. Точнее, он создаёт мутабельную копию навигейшн-стека, применяет к нему указанный блок, а затем сохраняет результат через `-⁠setViewControllers:animated:`.

Частный случай — замена или удаление конкретного контроллера в стеке, это делается через метод `-⁠srf_replaceViewController:withViewController:animated:`. Вместо второго контроллера можно передать `nil`, тогда произойдёт удаление первого контроллера.

Как правило, не рекомендуется проектировать интерфейс так, чтобы приходилось заменять уже добавленные контроллеры. Лучше вместо этого изменять состояние уже добавленных контроллеров.

### UIRefreshControl+SRFConvenientSwitching

Что не так с обычным `UIRefreshControl`.

* Приходится использовать два отдельных метода `-⁠beginRefreshing` и `-⁠endRefreshing` для переключения состояния. Хотелось бы, чтобы уже существующее свойство `refreshing` было readwrite.
* В зависимости от состояния контроллера, мы можем отобразить контент, не подразумевающий жеста pull-to-refresh. При этом приходится удалять существующий рефреш-контрол и либо хранить на него сильную ссылку, либо пересоздавать его потом заново. Хотелось бы иметь булево readwrite-свойство типа `enabled` для таких вещей.
* У `UIScrollView` нет свойства, позволяющего получить его рефреш-контрол. Что странно, поскольку два рефреш-контрола на одном скролле смысла не имеют.

Эта утилита добавляет два соответствующих булевых readwrite-свойства, а также способ получить рефреш-контрол для указанного скролла.

* Свойство `refreshing`, уже доступное для чтения, делается доступным для записи. При присовении вызывается соответствующий метод (`-⁠beginRefreshing` или `-⁠endRefreshing`).
* Свойство `srf_disabledInScrollView` (`NO` по умолчанию) добавляет / удаляет данный рефреш-контрол на его скролл, однако скролл продолжает быть владельцем рефреш-контрола.
* Для `UIScrollView` добавляется свойство `srf_attachedRefreshControl`. При присваивании рефреш-контрол сам добавляется на скролл. Эта ссылка не обнуляется при присваивании `srf_disabledInScrollView = YES`. Таким образом, нет необходимости хранить рефреш-контрол в отдельном свойстве контроллера.

Пример использования.

```objc
// во время viewDidLoad
self.scrollView.srf_attachedRefreshControl = [UIRefreshControl new];

// при обновлении UI
BOOL pullToRefreshEnabled = ...;
BOOL currentlyRefreshing = ...;
self.scrollView.srf_attachedRefreshControl.srf_disabledInScrollView = !pullToRefreshEnabled;
self.scrollView.srf_attachedRefreshControl.refreshing = currentlyRefreshing && pullToRefreshEnabled;
```

### UIScrollView+SRFImmediateTouches

Суть проблемы. `UIScrollView`, настроенная по умолчанию, задерживает тачи, пока не убедится, что это скорее тап по кнопке, нежели начало свайпа. Так что быстрый тап по кнопке не приводит к её подсветке, что в определённых ситуациях нежелательно. Проблема изложена на [корпоративной wiki](http://wiki.surfstudio.ru/Reusable_iOS_Code#UIScrollView.2BSRFImmediateTouches) и на [stackoverflow](http://stackoverflow.com/questions/19256996/uibutton-not-showing-highlight-on-tap-in-ios7).

Утилита добавляет к скроллу булево readwrite-свойство `srf_immediateTouches`. Его можно присвоить в вёрстке (через [user-defined runtime attributes](https://developer.apple.com/library/mac/recipes/xcode_help-interface_builder/Chapters/AddUserDefinedRuntimeAttributes.html)) либо в рантайме. При присвоении в `YES` происходит присвоение `delaysContentTouches = NO` для самого скролла и для вложенных скрытых скроллов, присутствующих у `UITableView` и `UICollectionView`.

### UIScrollView+SRFScrollToView

Метод `-⁠srf_scrollViewToVisible:animated:` позволяет проскроллить `UIScrollView` так, чтобы стало видно конкретную вьюху (в отличие от метода `-⁠scrollRectToVisible:animated:`, которому нужно указать прямоугольник).

	[self.scrollView srf_scrollViewToVisible:self.someButton animated:YES];

Если скролл нужно выполнить в рамках какой-то своей анимации, вызываем в блоке анимации с параметром `animated = NO`.

Если есть несколько элементов, которые надо показать, то вызываем тот же метод для каждой вьюхи по повышению приоритета:

	[UIView animateWithDuration:0.5 animations:^{
		//сначала пытаемся показать всю секцию
	    [self.scrollView srf_scrollViewToVisible:self.submitSection animated:NO];
	    //если не влезает вся секция, то показываем хотя бы кнопку "Отправить"
	    [self.scrollView srf_scrollViewToVisible:self.submitButton animated:NO];
	}];

### UIView+SRFHidingWithConstraints

Умеет скрывать вьюхи путём добавления констрейнта с нулевой высотой или шириной. Незаменим для вёрстки больших форм, где некоторые элементы могут исчезать и появляться, в т. ч. анимированно.

Соответствующий констрейнт добавляется / удаляется при присвоении булевых readwrite-свойств `srf_hiddenWithWidthConstraint` и `srf_hiddenWithHeightConstraint`, работающих для любой вьюхи.

* Верстаем содержимое формы со всеми показанными элементами.
* Для каждого скрываемого элемента высота должна быть определена изнутри цепочкой констрейнтов, соединяющих верх и низ. Частный случай — просто фиксированная высота.
* Хотя бы один констрейнт этой цепочки (лучше всего — самый нижний) должен иметь приоритет меньше 1000, чтобы не было конфликтов с констрейнтом нулевой высоты. При этом он будет отображаться пунктирной линией.
* Чтобы спрятать элемент (во `-⁠viewDidLoad`, например), присваиваем `myView.hiddenWithHeightConstraint = YES` (для горизонтальной последовательности используем `hiddenWithWidthConstraint`).
* Чтобы показать элемент обратно, присваиваем `myView.hiddenWithHeightConstraint = NO`.
* Чтобы показать/скрыть элемент с анимацией, делаем соответствующее присвоение внутри блока анимации.

Пример 1. Анимированный показ / сокрытие элемента, свёрстанного в вертикальной последовательности элементов.

```objc
[UIView animateWithDuration:0.5 animations:^{
    self.foldableSubview.srf_hiddenWithHeightConstraint = !shouldShowFoldableSubview;
    [self.view layoutIfNeeded];
}];
```

Пример 2. Создаём всплывающее окно из ксибы, анимированно показываем, ждём три секунды, анимированно прячем.

```objc
UIView *popup = [[NSBundle mainBundle] loadNibNamed:@"MyPopup" owner:nil options:nil].firstObject;
[self.view addSubview:popup];
[popup mas_makeConstraints:^(MASConstraintMaker *make) {
	//растягиваем вьюху по верхнему, левому и правому краю
    make.top.and.leading.and.trailing.equalTo(self.view);
    //высоту ставим, как в ксибе, но с неабсолютным приоритетом
    make.height.equalTo(@(popup.frame.size.height)).priority(999);
}];
//изначально прячем
popup.srf_hiddenWithHeightConstraint = YES;
[self.view layoutIfNeeded];
//анимированно показываем
[UIView animateWithDuration:0.5 animations:^{
	popup.srf_hiddenWithHeightConstraint = NO;
    [self.view layoutIfNeeded];
} completion:^(BOOL finished) {
	//ждём три секунды и анимированно прячем
    [UIView animateWithDuration:0.5 delay:3 options:0 animations:^{
        popup.srf_hiddenWithHeightConstraint = YES;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [popup removeFromSuperview];
    }];
}];
```

### UIView+SRFKeyboardSizeSubscription

Метод `-⁠srf_subscribeToKeyboardSizeNotificationsWithAnimations:` позволяет менять размеры нужных вьюх (в том числе скролл-вьюхи) при показе / сокрытии клавиатуры.

В параметр `animations` нужно передать блок, меняющий размер нужным образом. Он будет выполняться в блоке анимации с нужными параметрами (длительность и тайминг), совпадающими с анимацией движения клавиатуры, так что движение будет выглядеть полностью синхронно.

В блок передаётся величина `overlappingHeight` — высота пересечения клавиатуры и нашей вьюхи. Самый простой способ использовать его — присвоить его в константу констрейнта, отделяющего низ контента от низа нашей вьюхи.

Подписка живёт, пока не умрёт вьюха, для которой оформлена подписка.

* Верстаем контент на вьюхе, которая будет ресайзится или перемещаться при появлении клавиатуры. Назовём её ползающей вьюхой. Это может быть скролл или просто подвижная форма.
* Помещаем ползающую вьюху на некую неподвижную вьюху.
* Констрейнт, отделяющий нижний край ползающей вьюхи от нижнего края неподвижной вьюхи, выносим в аутлет.
* Во `-⁠viewDidLoad` вызываем у неподвижной вьюхи метод `-⁠srf_subscribeToKeyboardSizeNotificationsWithAnimations:`.
* В колбэке присваиваем `overlappingHeight` в константу нашего констрейнта.

```objc
- (void)viewDidLoad {
	...
    @weakify(self);
    [self.view subscribeToKeyboardSizeNotificationsWithAnimations:^(CGFloat overlappingHeight) {
        @strongify(self);
        self.keyboardHeightConstraint.constant = overlappingHeight;
    }];
    ...
}
```

Есть популярная либа IQKeyboardManager, которая, на первый взгляд, делает то же самое. Однако её цель — непересечение клавиатуры с выбранным текстовым полем, а двигает она весь контроллер целиком. И не предоставляется никаких способов контролировать это движение.

Предполагается, что при правильном использовании UIView+SRFKeyboardSizeSubscription в принципе не возникает ситуаций, когда клавиатура накрывает текстовое поле, ибо сама форма не будет пересекаться с клавиатурой.

### UIViewController+SRFContainer

Чтобы внутри одного вью-контроллера показать другой, нужно в определённом порядке сделать ряд вещей, описанных в [гайдах Apple](https://developer.apple.com/library/ios/featuredarticles/ViewControllerPGforiPhoneOS/ImplementingaContainerViewController.html), а именно — построить иерархию вью-контроллеров, построить иерархию вьюх, правильно уведомить дочерний контроллер о начале и конце перехода.

С помощью данной утилиты эту последовательность можно выполнить одним методом `-⁠srf_addChildViewController:andShowInContainerView:`, а удалить дочерний контроллер обратно можно методом `-⁠srf_removeFromParentViewControllerAndHideView`.

Вьюха добавленного вью-контроллера добавляется на указанную вьюху (можно использовать как `self.view`, так и любую другую конкретную сабвьюху) и выравнивается по её размеру через четыре констрейнта (leading, trailing, top, bottom).

Отображение / сокрытие происходит мгновенно, т. е. события `-⁠willMoveToParentViewController:` и `-⁠didMoveToParentViewController:` вызываются синхронно.
